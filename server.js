"use strict";
// ================================================================================================
// Heroku does not like others IP Servers than '0.0.0.0' - NOT 'localhost' nor '127.0.0.1' or whatever!
// Check it @ https://help.heroku.com/P1AVPANS/why-is-my-node-js-app-crashing-with-an-r10-error
// ================================================================================================
const ONE_WEBPACK_HOST_2_PORTS = "0.0.0.0"

const WEBPACK_PORT = 8081;
// Heroku NEEDS the environement variable 'PORT'
const APP_SERVER_PORT = process.env.PORT || WEBPACK_PORT;

const SSP_PORT = 8080;                      // <= origin will be allowed @ 'sspServer' bootstrap below
// ================================================================================================
// AND Heroku NEEDS the environement variable 'PORT' - it's its OWN internal var.
// ================================================================================================
// const BID_SERVER_PORT = (process.env.PORT + 1) || SSP_PORT;
// I thought so... Heroku complains about the adding '+1' process... let's try a magical fixed one:
const BID_SERVER_PORT = 8080 || SSP_PORT;

// ================================================================================================

const REPORT_MAX_LINES = 5;

const reportPath = __dirname + '/public/reports/report.txt';
let reportLines = 0;
let reportContents = '';
// To report the winning bids, we'll use several (must be unique!) files, with a date + timeStamp constructed name, each with a REPORT_MAX_LINES line's content:
const getNowDateString = (now) => {
    const year = now.getFullYear();
    const month = `${now.getMonth() + 1}`.padStart(2, '0');
    const day = `${now.getDate()}`.padStart(2, '0');

    const formatToLocaleTime = {
        hour: '2-digit'
        , minute:'2-digit'
        , second: '2-digit'
        , hour12: false
    };
    const timeStamp = now.toLocaleTimeString([], formatToLocaleTime).replace(/:/g, '.') + '.' + now.getMilliseconds();

    return `${year}${month}${day}-${timeStamp}`;
}
// Browser's page has a button where user can stop the automatic refresh bid's request to here => stop this server, on that event
let isToCloseServer = false;

const express = require('express');
const fs = require('fs');
const colors = require('colors/safe');
const webpack = require('webpack');
const webpackDevServer = require("webpack-dev-server");

const webpackServer = (() => {
    const webpackConfig = {
        ...require("./webpack.dev.config"),
        ...{
            watch: false
        }
    };
    const compiler = webpack(webpackConfig);
    return new webpackDevServer(compiler, {
        https: false,
        stats: {
            colors: true,
            env: true
        },
        contentBase: __dirname + '/build-dev',
        watchContentBase: true,
        hot: false,
        hotOnly: false,
        host: ONE_WEBPACK_HOST_2_PORTS,
        port: APP_SERVER_PORT,
        disableHostCheck: true
    });
})();

webpackServer.listen(APP_SERVER_PORT, ONE_WEBPACK_HOST_2_PORTS, () => {
    console.log(colors.green(`Starting Webpack Development Server on ${ONE_WEBPACK_HOST_2_PORTS}:${APP_SERVER_PORT}`));
});

const sspServer = (() => {
    const app = express();
    const randomBetween = (min, max) => Math.floor(Math.random()*(max-min+1)+min);

    app.use((req, res, next) => {
        console.log(colors.cyan(`Request for "${req.url}"`));
        next();
    });
    app.use( (req, res, next) => {
        // Who's calling...?
        var origin = req.headers.origin;
        console.log(origin);

        // Prepare sending:
        res.header('Content-Type', 'application/json');
        // Must add:
        res.header('Access-Control-Allow-Headers', 'Content-type, ssp');

        // Control origin:
        // res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8080');
        const allowedOrigins = [
            `http://${ONE_WEBPACK_HOST_2_PORTS}:${WEBPACK_PORT}`,
            `http://0.0.0.0:${WEBPACK_PORT}`,
            `http://localhost:${WEBPACK_PORT}`,
            `http://127.0.0.1:${WEBPACK_PORT}`,
            // And, in production:
            `https://bid-add-server.herokuapp.com`,
            `http://bid-add-server.herokuapp.com`
        ]        
        
        // We could have here different origins... just stick each on next array (being filtered):
        if (allowedOrigins.indexOf(origin) > -1) {
            res.setHeader('Access-Control-Allow-Origin', origin);
        }

        next();
    });

    app.use( (req, res, next) => {
        // setTimeout(() => next(), randomBetween(100, 2000));
        setTimeout(() => next(), randomBetween(400, 1000));
    });

    app.use(express.json());

    // The effectively RENDERED winning bids at the browser, will be logged on a 'render-report-xxxxxx.txt' file.
    // The request comes from the browser, as soon as ANY bid is rendered:
    app.post('/send-report', (req, res) => {
        const rawReport = req.body.data;
        reportLines += 1;
        
        const serverDate = `ssp: ${rawReport.ssp} @ ${rawReport.time}`;
        const winningBid = rawReport.spanContent;
        const winningBidPlacement = rawReport.spanPlacement;

        const now = new Date();
        const reportFile = reportPath.replace('report.txt', `render-report-${getNowDateString(now)}.txt`);

        console.log(colors.yellow(serverDate));
        console.log(colors.magenta(winningBid + ' @ ' + winningBidPlacement));

        reportContents += serverDate + ' => ' + winningBid + ' @ ' + winningBidPlacement + '\n';

        // When to do the 'reportFile' file, with 'reportContents' being cumulated...?
        if (reportLines === REPORT_MAX_LINES || isToCloseServer) {
            fs.writeFileSync(reportFile, reportContents);
            console.log(colors.green(`Report "${reportFile}" was written @ /public/reports!`));

            if(isToCloseServer) {
                closeServer();
            } else {
                reportLines = 0;
                reportContents = '';
            }
        };
    });

    // Browser's page has a button where user can stop the automatic refresh bid's request to here.
    // Receive the event, do last file and close the server:
    app.post('/finish-bidding', (req, res) => {
        console.log(colors.bgRed('USER is CLOSING down the SSP Bid Server... finish last tasks.'));
        isToCloseServer = true;
        // closeServer();   // <= there's still the file to do (if REPORT_MAX_LINES hasn't been reached)
    });

    // The requests to fetch bids onto this SSP Server (a, b , c, d, etc.)
    app.post('*', (req, res) => {
        const seg = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        const creativeId = `${seg()}-${seg()}-${seg()}-${seg()}-${seg()}`;
        const payload = req.body;
        const size = payload.sizes? payload.sizes[randomBetween(0, payload.sizes.length-1)]: [1,1];
        const ssp = req.headers.ssp;
        const cpm = Math.floor(Math.random() * 399) / 100;
        // Place random img at the background so we can actually see which container is changing/made a winning bid, if any:
        // (remember that now we have timeouts; might happen some containers will NOT refresh an ad!)
        // ===============================================================================
        const imgSrc = `https://source.unsplash.com/collection/190727/${size[0]}x${size[1]}`;
        const bgImgCSS = `background-image: url('${imgSrc}');background-position:center;background-repeat:no-repeat`;
        const fontContrastCSS = `color: white; text-shadow: 1px 7px 7px black, 0 0 25px blue, 0 0 5px darkblue;`;
        // ===============================================================================
        const adCode = `<script>document.write('`
                        + `<div style="width:${size[0]}px;height:${size[1]}px;background:#EEEEEE;${bgImgCSS};text-align:center;">`
                        +   `<span style="${fontContrastCSS}">${creativeId} @ ${size.join("x")} @ ${cpm.toFixed(2)}</span>`;
                        + `</div>'`
                        + `);</script>`
        ;

        const objToSend = {
            "adUnitCode": payload.adUnitCode,
            "creativeId": creativeId,
            "ssp": ssp,
            "size": size,
            "cpm": cpm,
            "ad": adCode.trim(),
            "imgAd": imgSrc,
            "isDeal": randomBetween(1,10) === 5
        };
        res.send(JSON.stringify(objToSend));
    });

    return app;

})();

const nodeServer = sspServer.listen(BID_SERVER_PORT, ONE_WEBPACK_HOST_2_PORTS, () => {
    console.log(colors.green(`Starting SSP Bid Server on ${ONE_WEBPACK_HOST_2_PORTS}:${BID_SERVER_PORT}`));
});

const closeServer = () => {
    nodeServer.close();
    console.log(colors.bgRed('... and Node Server has been CLOSED.'));
}
