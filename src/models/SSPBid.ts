import { Bid, BidResponseData } from "./abstract/Bid";

export class SSPBid extends Bid {

    size: string;
    width: number;
    height: number;
    cpm: number;
    ssp: string;
    creativeId: string;
    ad: string;
    imgAd: string;
    isDeal: boolean;
    adUnitCode: string;

    constructor(bid: BidResponseData){

        /*
            TODO

            Pass the bid object returned by the SSP
         */
        super();

        this.size = bid.size;
        this.width = bid.width;
        this.height = bid.height;
        this.cpm = bid.cpm;
        this.ssp = bid.ssp;
        this.creativeId = bid.creativeId;
        this.ad = bid.ad;
        this.imgAd = bid.imgAd;
        this.isDeal = bid.isDeal;
        this.adUnitCode = bid.adUnitCode;

    }

    getSize(): string {
        return this.size;
    };

    getWidth(): number {
        return this.width;
    }

    getHeight(): number {
        return this.height;
    }

    getCPM(): number {
        return this.cpm;
    }

    getSSP(): string {
        return this.ssp;
    }

    getCreativeId(): string {
        return this.creativeId;
    }

    getAd(): string {
        return this.ad;
    }

    getAdImgUrl(): string {
        return this.imgAd;
    }

    getIsDeal(): boolean {
        return this.isDeal;
    }

    getAdUnitCode() : string {
        return this.adUnitCode;
    }

}

export class RenderReport {
    spanContent: string;
    spanPlacement: string;
    ssp: string;
    time: string;
}