export interface AdManagerConfig {
    endPoint: string;
    reloadAdTime: { ms: number };
    isFetchTimeout: { active: boolean, ms: number };
    isThroughFetchService: boolean;
    isForkJoinRequest: boolean;
}

export interface BidResponseData {
    readonly size: string;
    readonly width: number;
    readonly height: number;
    readonly cpm: number;
    readonly ssp: string;
    readonly creativeId: string;
    readonly ad: string;
    readonly imgAd: string
    readonly isDeal: boolean;
    readonly adUnitCode: string;
}

export abstract class Bid {

    abstract getSize(): string;

    abstract getWidth(): number;

    abstract getHeight(): number;

    abstract getCPM(): number;

    abstract getSSP(): string;

    abstract getCreativeId(): string;

    abstract getAd(): string;

    abstract getAdImgUrl(): string;

    abstract getIsDeal(): boolean;

    abstract getAdUnitCode(): string;

}

export interface BidFetchData {
    ssp: string;
    payload: BidRequestPayload;
    varName: string;
    varValue: Object | null// The 'fetch' JSON response
}

export interface BidFetchParams {
    method: string;
    headers: Headers;
    body: string;
}

export interface BidRequestPayload {
    adUnitCode: string;
    sizes: Array<Array<string>>
}



