import { SSPBid } from "../models/SSPBid";

export class BidResponse {

    bids: SSPBid[];

    constructor(bids: SSPBid[]) {
        this.bids = bids;
    }

    getWinningBid(): SSPBid {

        /*
            TODO

            Given a set of winning bids, should return the highest value bid.

            If this is a "deal" then we should prioritise this over other bids, regardless of other non-deal bid values, selecting the best "deal".
         */
        console.warn('Array of BIDS', this.bids);

        let winningBid = {} as SSPBid;
        
        // Search for 'isDeal' and check if there's others:
        let foundIsDeal = [] as Array<SSPBid>;
        for (const bid of this.bids) {
            if(bid.isDeal) { 
                console.error(`======= Call to ssp "${bid.ssp}" has a DEAL (cpm = ${bid.cpm})! =======`);
                foundIsDeal.push(bid);
            }
        }

        // Do we have at least one 'IsDeal' bid?
        if (foundIsDeal.length > 0) {
            // Is it unique?
            if(foundIsDeal.length === 1) {
                winningBid = foundIsDeal[0];
            } else {
                // Find the highest 'cpm'  @ 'foundIsDeal'
                winningBid = this.findMaximum(foundIsDeal).obj;
            }
        
        // No 'isDeal' => find the highest 'cpm' @ 'this.bids'
        } else {
            winningBid = this.findMaximum(this.bids).obj;
        }

        return winningBid;

    }

    findMaximum(inArray: SSPBid[]) {
        let maxCPM = 0;
        let objMaxCPM = {} as SSPBid;
        inArray.forEach(
            (eachBid) => {
                if (eachBid.cpm > maxCPM) {
                    maxCPM = eachBid.cpm
                    Object.assign(objMaxCPM, eachBid); 
                }
            }
        );

        return {obj: objMaxCPM, val: maxCPM};
    }

}