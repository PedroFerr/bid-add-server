import { AdManagerConfig, BidFetchData } from '../models/abstract/Bid';

import { BidFetch } from './BidFetch';
import { FetchService } from './BidTimeoutFetch';

export class BidRequest {

    strConsole = '';

    constructor(private configJSON: AdManagerConfig) {
        if (!this.configJSON.isThroughFetchService) {
            this.strConsole = 'Forked Timeout Asynchronous Fetch is OFF - using "BidFetch" Class';
        } else {
            this.strConsole = 'Forked Timeout Asynchronous Fetch is ON - using "FetchService" Class';
        }
        this.echoAtConsole(this.strConsole);
    }

    /*
        TODO

        For each SSP on the placement (data-ssps), make a request to bid on the sizes (data-sizes).

     */

    /**
     * Find and extract, the data tags of our HTML containers.
     * Each tag will fire bid requests, to our Node server, for each found 'ssp' @ 'data-ssps' string list
     */
    snifDOMtoDoRequests(placementTag: HTMLElement, tagIndex: number) {

        let requests: Array<BidFetchData> = [];
        // For the various SSPs:
        const placementSSPs: string = placementTag.getAttribute('data-ssps');
        // For the various sizes, each with 2 measurements 'inside':
        const
            placementSizes: string = placementTag.getAttribute('data-sizes')
            , aTemp = placementSizes.split(',')
            
        ;
        let aMeasures: Array<Array<string>> = [];
        aTemp.forEach(
                (size: string) => aMeasures.push( size.split('x') )
            )
        ;
        
        // The array of requests is a combination of ALL found sizes, on each HTML container data tag, for EACH SSP.
        // Besides we just add 'varName' and 'varValue' so, we do an array of returned Promise objects from fetching all requests, in a tag container, at once:
        const arraySSPs = placementSSPs.split(',');
        for (const eachSSP of arraySSPs) {
            requests.push(
                {
                    ssp: eachSSP
                    , payload: {
                        adUnitCode: `tag-${tagIndex}-fetch-${eachSSP}`
                        , sizes: aMeasures
                    }
                    , varName: `tag-${tagIndex}-fetch-${eachSSP}`
                    , varValue: null
                }
            );
        }
    
        // OLD CODE
        if (!this.configJSON.isThroughFetchService) {
            // OK. Fire for each 'ssp', or all, so 'BidResponse' can select a winner @ 'getWinningBid()' for this 'placementTag':
            // **********************
            const bidFetch = new BidFetch(this.configJSON.endPoint);

            if(this.configJSON.isForkJoinRequest) {
                bidFetch.doForkJoinFetch(requests);
            } else {
                bidFetch.doFetch(requests);
            }
            // **********************

        // NEW Promiss.all CODE
        } else {
            // Let's do it with our brand newly amazingly brilliant 'Forked Timeout Asynchronous Fetchs':
            const authHeader = new Headers();
            authHeader.append( 'Content-type', 'application/json');
            authHeader.append('ssp', 'sspa');  
    
            // **********************
            const fetchService = new FetchService(this.configJSON.endPoint, requests);
            // Set the fetch with a timeout parametrized... or not! Just put 'configJSON.isFetchTimeout.active' into 'false' @ AdManagerConfig:
            // Even so, if you want to test the old code @ BidFetch.ts, just comment these block lines and uncomment the upper block.
            fetchService.withTimeout = { active: this.configJSON.isFetchTimeout.active, ms: this.configJSON.isFetchTimeout.ms };
            fetchService.run();
            // **********************
        }
    }

    echoAtConsole(str: string) {
        console.log('###-###-###-###-###-###-###-###-###-###');
        console.error(str);
        console.log('###-###-###-###-###-###-###-###-###-###');
        console.warn(this.configJSON);
    }

}