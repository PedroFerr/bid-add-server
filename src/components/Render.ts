import { SSPBid, RenderReport } from "../models/SSPBid";

export class Render {

    constructor(private endPoint: string) {}

    renderIt(bid: SSPBid): Promise<string> {
        const _bid = new SSPBid(bid);
        const id = _bid.getCreativeId();

        console.log('WINING Bid to Render: ', id, bid);

        return new Promise((resolve, reject) => {
            /*
                TODO

                Render the ad-code, if it success, resolve, otherwise reject...

                Think about browsers, especially older browsers and different ways of loading javascript in.

                Bonus points for adding multiple ad rendering methods.
             */

            // Nice article about Promisses:
            // https://javascript.info/promise-basics

            // Nice explanation about the problems of loading a script dynamically
            // @ https://www.danielcrabtree.com/blog/25/gotchas-with-dynamically-adding-script-tags-to-html
            // Scripts are loaded asynchronously - they cannot call document.write
            //
            // And EXCELENT dissertation about everything going on SCRIPTS:
            // @ https://www.html5rocks.com/en/tutorials/speed/script-loading/
            // "IE loads scripts differently to other browsers."

            // _bid.getAd() = // "<script>document.write('<div style="width:970px;height:90px;background:#EEEEEE">2d3b-b435-c387-2fb0-828c @ 970x90 @ 1.60</div>');</script>"

            // const scriptToLoad = _bid.getAd().replace('<script>', '').replace('</script>', '');
            // const newScript = document.createElement("script");
            // const inlineScript = document.createTextNode(scriptToLoad)
            // newScript.appendChild(inlineScript); 

            const bidDivContent = _bid.getAd().replace('<script>document.write(\'', '').replace('\');</script>', '');
            const bidGroupIdx = Number(_bid.getAdUnitCode().split('-')[1]);
            const requestedSSPserver = _bid.getSSP();

            // Remove old bids from DOM - for now just this coming 'bidGroupIdx' - so new one can take its place:
            // BUT...
            // in order not to stay with an 'empty' markup, we want, first, to preload the becoming background img of this winning bid
            const imgToBgdURL = _bid.getAdImgUrl();
            const imgPreLoader = new Image();
            imgPreLoader.src = imgToBgdURL;
            // => use HTML5's 'load' callback when img is ready:
            imgPreLoader.onload = () => {

                this.imgIsLoaded(bidGroupIdx, bidDivContent, (renderedBidStr: string): any => {
                    const renderedStr = renderedBidStr.split('<span')[1].split('>')[1].split('</span')[0];
                    const now = new Date();
                    let pagePlace = ''
                    switch (bidGroupIdx) {
                        case 0:
                            pagePlace = 'top'
                            break;
                        case 1:
                            pagePlace = 'left-top'
                            break;
                        case 2:
                            pagePlace = 'left-bottom'
                            break;
                    
                        default:
                            pagePlace = 'ERROR: no container placement found!'
                            break;
                    }

                    // Finally:
                    const reportObj: RenderReport = {
                        spanContent: renderedStr
                        , spanPlacement: pagePlace
                        , ssp: requestedSSPserver
                        , time: now.toLocaleTimeString() + '.' + now.getMilliseconds()
                    };
                    //... which will be sent to:
                    const reportURL = `${this.endPoint}/send-report`;
                    // ... with next params:
                    const reqParams =  {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                            , 'ssp': bidDivContent
                        },
                        body: JSON.stringify({data: reportObj})
                    };

                    // console.error('---------------------------------------------')
                    // console.log('RENDERED Bid was:', renderedStr);
                    // console.error('---------------------------------------------')
                    
                    fetch(reportURL, reqParams);
                });    
            };
            // Might happen:
            imgPreLoader.onerror = () => {
                console.error(`ERROR loading "${imgToBgdURL}" img for "${_bid.getAdUnitCode()}"`);
                reject(_bid.getAdUnitCode());
            };

            resolve(id);
        });
    }

    // Once our wining bid background image is loaded into the DOM,
    // we can remove old and apply the new bid - preloaded img will render instantaneously, on the layers switching:
    imgIsLoaded(imgIdx: number, bidDivContent: string, callback: (bidHasBeenRenderd: string) => any) {
        
        const placementContainers = document.querySelectorAll('.placement-container');
        const oldBidContainer = placementContainers[imgIdx].nextSibling;
        if (oldBidContainer) {
            oldBidContainer.parentNode.removeChild(oldBidContainer);
        }

        // Create the DOM's node to hold this new coming winning bid:
        const bidDivContainer = document.createElement('div');
        bidDivContainer.classList.add('bid-container');

        bidDivContainer.style.overflow = 'hidden';
        bidDivContainer.style.display = 'flex';
        // Some CSS fade-in/out effect, to smooth the born of the new 'bid-container':
        bidDivContainer.style['-webkit-transition'] = 'all 0.5s ease-out';
        bidDivContainer.style['-moz-transition'] = 'all 0.5s ease-out';
        bidDivContainer.style['-o-transition'] = 'all 0.5s ease-out';
        bidDivContainer.style.transition =  'all 0.5s ease-out';
        // ... and place it inside, so far with '0' opacity:
        bidDivContainer.style.opacity = '0';
        bidDivContainer.innerHTML = bidDivContent;
        
        // Append it to the correct container:
        const addsContainers = document.getElementsByClassName('card-block');
        const thisAdContainer = addsContainers[imgIdx];
        thisAdContainer.appendChild(bidDivContainer);
        // Bring him to life, smoothly:
        setTimeout(() => bidDivContainer.style.opacity = '1', 0);

        // We can now be assured to say to the Node server that THIS bid was ACTUALLY rendered:
        // (pulling it out from previos 'BidTimeoutFetch' Class, wouldn't assure that the winning bid was REALLY rendered at the browser!)
        callback(bidDivContent);
    }

}