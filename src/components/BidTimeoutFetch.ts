"use strict";

import { BidFetchData, BidFetchParams } from '../models/abstract/Bid';
import { SSPBid } from '../models/SSPBid';

import { BidResponse } from './BidResponse';
import { Render } from './Render';


interface TimeoutSwitch {
    active: boolean;
    ms: number;
}

/************************************************
 * Asynchronous functions with async and await.
 ***********************************************/
export class FetchService {
    
    _timeout = {} as TimeoutSwitch;

    constructor(
        private endPoint: string,
        private requests: Array<BidFetchData>
    ) {
        Object.assign(this._timeout, { active: false, ms: 0 });
    }

    /************************************************************************************
     * Provide a fetch based promise and set a initiation timer. The Fetch API currently
     * has not mechanism to handle timed out AJAX requests. Here's an explanation from
     * one of the Fetch API maintainers (mislav) on Github:
     *
     * Note that this is not a connection timeout. This is a response timeout. Due to
     * technical restrictions we can't implement a connection timeout. Also note that with
     * the above implementation, even if the timeout happens, the original request won't
     * be aborted because we don't have an API to abort fetch requests. Instead, the
     * request will complete in the background but its response will get discarded.
     *
     * What this function provides is an initiation timeout. If the Fetch request takes
     * more than a set amount of time in ms then reject that request's response data.
     *
     * See: https://github.com/github/fetch/issues/175
     *
     * @param fetchPromise
     * @returns {Promise}
     */
    fetchWithTimeout(fetchPromise: Promise<any>): Promise<any> {
        if (this._timeout.active && this._timeout.ms > 0) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    reject(new Error(`Fetch is taking too long (>${this._timeout.ms}ms!). Rejecting the response.`));
                }, this._timeout.ms);

                fetchPromise.then(resolve, reject);
            });
        } else {
            console.warn("_timeout member needs to be active with seconds greater than zero");
        }
    }

    // Activate, and set timeout value, from the outside:
    set withTimeout(timeout: TimeoutSwitch) {
	    this._timeout = JSON.parse(JSON.stringify(timeout)); // deep copy
    }

    async asyncFetchData(): Promise<Array<SSPBid>> {
        // Do an array of Promise objects, so we fetch for all requests, at once, in a tag container:
        let combinedDataObj = {};
        let arrayVars = [];

        for (let eachReq of this.requests) {
            // For NOT to accumulate headers values, on each cycle, 'headers' SHOULD be reseted!
            // That's why we put inside the cycle 'new Headers()' and not at the begining of this method.
            const authHeader = new Headers();
            // Check headers are authorized @ our Node server coding!
            authHeader.append( 'Content-type', 'application/json');
            authHeader.append('ssp', eachReq.ssp);    // sspa,sspb,sspc,sspd,sspe,sspf
            
            const requestParams = {
                method: 'POST',
                headers: authHeader,
                body: JSON.stringify(eachReq.payload)
            };
    
            eachReq.varValue = await (
                (this._timeout.active && this._timeout.ms > 0) ?
                    this.fetchWithTimeout( this.doOrdinaryFetch(requestParams, eachReq.payload.adUnitCode) )
                    :
                    ( this.doOrdinaryFetch(requestParams, eachReq.payload.adUnitCode) )
            );
            
            // Initialize 'combinedDataObj', for each request/var
            combinedDataObj[eachReq.varName] = {};
            // Do a simple array with all the request vars value:
            arrayVars.push(eachReq.varValue);
        };

        // Next resolves when ALL of the promises in the iterable argument have resolved,
        // or rejects, with the reason of the first passed promise that rejects.
        const forkedResponse = await Promise.all(arrayVars)
            .then( (values) => {
                for (let i = 0; i < this.requests.length; i++) {
                    combinedDataObj[this.requests[i].varName] = values[i];
                }

                // Transform into an Array:
                const responsesArray = [] as Array<SSPBid>;
                for (const key in combinedDataObj) {
                    if (combinedDataObj.hasOwnProperty(key)) {
                        const bidObj: SSPBid = combinedDataObj[key];
                        responsesArray.push(bidObj);
                    }
                }

                // And tht's it! We just forked all responses of each request:
                return responsesArray;
            })
        ;
        return await forkedResponse;

    }

    // Run the data fetch function.
    run() {
        this.asyncFetchData()
   	        .then(data => {
                // console.log("Data has arrived: ", data);
                
                // And, for each response of the HTML container group, filter to who's the winner:
                const bidResponse = new BidResponse(data);
                const bidWinner = bidResponse.getWinningBid();

                // And, finally, send it to render;
                const render = new Render(this.endPoint);                
                render.renderIt(bidWinner);

            })
		    .catch(reason => console.error("Caught error: ", reason.message));
    };

    // =======================
    //     AUX methods
    // =======================

    doOrdinaryFetch(requestParams: BidFetchParams, strId: string) {

        return(
            fetch(this.endPoint, requestParams)
                .then( (response) => response.json() )
                .catch((err: Error) => {
                    console.error(`Fetch to "${strId}" bid server ERROR: ${err}`);
                })
        );
    }
}