"use strict";

import { BidFetchData, BidFetchParams } from '../models/abstract/Bid';
import { SSPBid } from '../models/SSPBid';

import { BidResponse } from './BidResponse';
import { Render } from './Render';

/************************************************
 * Fetchs are sequential - next is only called after previous responds.
 ***********************************************/
export class BidFetch {


    constructor(private endPoint: string) {}

    /**
     * There is a bid server running on localhost:8081.
     * Set the SSP header to 'ssp: rubicon' and pass it to the server with sizes to bid on using, like i.e.: -
        {
            "adUnitCode": "abc123",
	        "sizes": [[300,250],[300,600]]
        }

        And it will mock up a bid that looks like: -
        {
            "adUnitCode": "abc123",
            "creativeId": "f28b-584b-868e-45a8-7e27",
            "ssp": "sspa",
            "size": [
                300,
                600
            ],
            "cpm": 1.79,
            "ad": "<div id=\"f28b-584b-868e-45a8-7e27\" style=\"width: 300px; height:600px; color:#EEEEEE\">\n                <script>document.querySelector(\"#f28b-584b-868e-45a8-7e27\").innerHTML(\"f28b-584b-868e-45a8-7e27 @ 300x600 @ 1.79\")</script>\n            </div>"
        }
     */

    doFetch(requests: Array<BidFetchData>) {
        const responsesArray = [] as Array<SSPBid>;

        requests.forEach( (eachReq, idx) => {
            // For NOT to accumulate headers values, on each cycle, 'headers' SHOULD be reseted!
            // That's why we put inside the cycle 'new Headers()' and not at the begining of this method.
            const authHeader = new Headers();
            // Check headers are authorized @ our Node server coding!
            authHeader.append( 'Content-type', 'application/json');
            authHeader.append('ssp', eachReq.ssp);    // sspa,sspb,sspc,sspd,sspe,sspf
            
            const requestParams = {
                method: 'POST',
                headers: authHeader,
                body: JSON.stringify(eachReq.payload)
            };

            fetch(this.endPoint, requestParams)
                .then( (response) => response.json() )
                .then( (data) => {
                    console.log(`Fetch to "${eachReq.payload.adUnitCode}"`, data);
                    responsesArray.push(data);

                    if(idx === requests.length - 1) {

                        // And, for each response of the HTML container group, filter to who's the winner:
                        const bidResponse = new BidResponse(responsesArray);
                        const bidWinner = bidResponse.getWinningBid();
                        
                        // And, finally, send it to render;
                        const render = new Render(this.endPoint);                
                        render.renderIt(bidWinner);
                    }
        
                })
                .catch((err: Error) => {
                    console.error(`Fetch to "${eachReq.payload.adUnitCode}" bid server ERROR: ${err}`);
                })
            ;    
        });

    }

    doForkJoinFetch(requests: Array<BidFetchData>) {


        // Do an array of Promise objects, so we fetch for all requests, at once, in a tag container:
        let combinedDataObj = {};
        let arrayVars = [];

        for (let eachReq of requests) {

            // For NOT to accumulate headers values, on each cycle, 'headers' SHOULD be reseted!
            // That's why we put inside the cycle 'new Headers()' and not at the begining of this method.
            const authHeader = new Headers();
            // Check headers are authorized @ our Node server coding!
            authHeader.append( 'Content-type', 'application/json');
            authHeader.append('ssp', eachReq.ssp);    // sspa,sspb,sspc,sspd,sspe,sspf
            
            const requestParams = {
                method: 'POST',
                headers: authHeader,
                body: JSON.stringify(eachReq.payload)
            };
    
            // Do each fetch and save it on our array indexed element:
            eachReq.varValue = fetch(this.endPoint, requestParams)
                .then( (response) => response.json() )
                .catch((err: Error) => {
                    console.error(`Fetch to "${eachReq.payload.adUnitCode}" bid server ERROR: ${err}`);
                })
            ;
            // Initialize 'combinedDataObj', for each request/var
            combinedDataObj[eachReq.varName] = {};
            // Do a simple array with all the request vars value:
            arrayVars.push(eachReq.varValue);
        }

        // Next resolves when ALL of the promises in the iterable argument have resolved,
        // or rejects, with the reason of the first passed promise that rejects.
        Promise.all(arrayVars)
            .then( (values) => {
                for (let i = 0; i < requests.length; i++) {
                    combinedDataObj[requests[i].varName] = values[i];
                }
                // Transform into an Array:
                const responsesArray = [] as Array<SSPBid>;
                for (const key in combinedDataObj) {
                    if (combinedDataObj.hasOwnProperty(key)) {
                        const bidObj: SSPBid = combinedDataObj[key];
                        responsesArray.push(bidObj);
                    }
                }
                
                // And, for each response of the HTML container group, filter to who's the winner:
                const bidResponse = new BidResponse(responsesArray);
                const bidWinner = bidResponse.getWinningBid();

                // And, finally, send it to render;
                const render = new Render(this.endPoint);                
                render.renderIt(bidWinner);
            })
        ;
    }
   
}
