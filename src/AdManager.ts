import { AdManagerConfig } from './models/abstract/Bid';

import { BidRequest } from './components/BidRequest';

export class AdManager {


    static load(config: AdManagerConfig): void {

        /*

        TODO

        On load, the ad-manager should look for ad-units on the page and ask the ad-server for bids...

        There is a bid server running on localhost:8081.
        Set the SSP header to 'ssp: rubicon' and pass it to the server with sizes to bid on using, like i.e.: -
        {
            "adUnitCode": "abc123",
	        "sizes": [[300,250],[300,600]]
        }
        
        And it will mock up a bid that looks like: -
        {
            "adUnitCode": "abc123",
            "creativeId": "f28b-584b-868e-45a8-7e27",
            "ssp": "sspa",
            "size": [
                300,
                600
            ],
            "cpm": 1.79,
            "ad": "<div id=\"f28b-584b-868e-45a8-7e27\" style=\"width: 300px; height:600px; color:#EEEEEE\">\n                <script>document.querySelector(\"#f28b-584b-868e-45a8-7e27\").innerHTML(\"f28b-584b-868e-45a8-7e27 @ 300x600 @ 1.79\")</script>\n            </div>"
        }
         */

        const bidRequest = new BidRequest(config);
        
        // For each found container tag, we will fire an 'ssp' group of bid requests, to our Node server, for each found 'ssp' @ 'data-ssps' string list:
        const addsContainers = document.querySelectorAll('.placement-container');
        for (let idx = 0; idx < addsContainers.length; idx++) {
            // Fetch bids into the Node Server:
            const bidGroup = addsContainers[idx] as HTMLElement;
            bidRequest.snifDOMtoDoRequests(bidGroup, idx);
        }
    }

    // User can click on top bar menu orange's button to stop the refreshment of the 'placement-containers' bids
    // => stoping the Node Server!
    static listenToStopServer() {
        const stopButton = document.getElementById("stop-auto-refresh") as HTMLElement;
        stopButton.addEventListener("click", (evt) => {
            evt.stopImmediatePropagation();
            clearInterval(interval);
            this.stopServerAtConsoleAffairs(stopButton);
        });
    }

    // =======================
    //     AUX methods
    // =======================

    // A few things have to be done, when user presses 'Stop AUTO refresh' button:
    static stopServerAtConsoleAffairs(stopButton: HTMLElement) {

        console.error('********************************************')
        console.warn('WARING: refreshment (next) cycle Bid has been STOPPED!');
        console.error('********************************************')
        
        stopButton.textContent = 'AUTO refresh STOPPED!';
        stopButton.classList.remove('btn-warning');
        stopButton.classList.add('btn-danger');
        stopButton.setAttribute('disabled', 'true');
        stopButton.style.cursor = 'not-allowed';
    
        // Send an order to close the Node Server - do not accept new connections:
        fetch(`${adManConfig.endPoint}/finish-bidding`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'}
        }); 
    }

    static echoRefreshAtConsole() {
        console.log('********************************************')
        console.error(`Ad group bid ${adManConfig.reloadAdTime.ms/1000}s REFRESH is about to be made.`);
        console.log('********************************************')
        console.log(' ');
        console.log(' ');
        console.log(' ');
    }

}

const ONE_WEBPACK_HOST_2_PORTS = "localhost"; // Won't run on "0.0.0.0"... 
const BID_SERVER_PORT = 8080;
// Up should be imported directly from 'server.js', like this:
// https://stackoverflow.com/a/10328308/2816279

const adManConfig = {

    endPoint: `http://${ONE_WEBPACK_HOST_2_PORTS}:${BID_SERVER_PORT}`
    
    // To make the most of our inventory we refresh ads **every 'reloadAdTime' mili seconds**:
    , reloadAdTime: { ms: 10000 }

    // Are you doing server calls within a certain imposed timeout?
    // Remember:
    //     * each group bid can have up to 6 'simultaneous' SSP requests...
    //     * give opportunity for each group bid / tag container to request to SSP Server
    //     * - having 3 bidding groups/tags on the page, total time (3 * ms) should be LESS than the reloading time (reloadAdTime)
    , isFetchTimeout: { active: false, ms: 3000 }

    // Are the requets using later developed code?
    // Meaning to use BidRequest or newere FetchService Classes..? Last with, or without, timeout - previous param
    , isThroughFetchService: false

    // Are the requests per SSP server of each '.placement-container', or a ForkJoin of ALL the SSPs on each group/'.placement-container'?
    , isForkJoinRequest : false
};

// Instantiate our adManager, with upper config setup:
AdManager.load(adManConfig);
// Start listening for user's refreshment stop button, at top bar menu:
AdManager.listenToStopServer();

// Launch an automatic refreshment of the AdManager class, so ads can periodically be fetched to the server,
// till 'stop AUTO refresh' button has been pressed by the user:
const interval = setInterval(() => {
    AdManager.echoRefreshAtConsole();
    AdManager.load(adManConfig);
}, adManConfig.reloadAdTime.ms);


